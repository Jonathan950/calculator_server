// Paso 2: Definir la implementación de la clase

package interfacesImp; //Agregar el paquete donde este la interfaz remota 

import Interfaces.Suma;// Importar la clase Adder para que pueda ser utilizada 

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class SumaImplementacion extends UnicastRemoteObject implements Suma {

    public SumaImplementacion() throws RemoteException {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    public int suma(int a, int b) throws RemoteException {
        return a + b;
    }
}
