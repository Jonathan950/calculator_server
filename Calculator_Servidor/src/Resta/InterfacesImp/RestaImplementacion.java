package InterfacesImp; //Agregar el paquete donde este la interfaz remota 

import Interfaces.Resta;// Importar la clase Adder para que pueda ser utilizada 

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class RestaImplementacion extends UnicastRemoteObject implements Resta {

    public RestaImplementacion() throws RemoteException {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    public int resta(int a, int b) throws RemoteException {
        return a - b;
    }
}
