package Interfaces; //Agregar el paquete donde este la interfaz remota 

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Resta extends Remote {
    public int resta(int a, int b) throws RemoteException;

}