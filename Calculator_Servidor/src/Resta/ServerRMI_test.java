
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import InterfacesImp.RestaImplementacion;

public class ServerRMI_test {
    public static void main(String[] arg) {
        try {
            RestaImplementacion restaImplementacion = new RestaImplementacion();
            Registry registry = LocateRegistry.createRegistry(9090);
            registry.rebind("Resta", restaImplementacion);
            System.out.println("Servidor corriendo");
        } catch (RemoteException e) {
            System.out.println(e.getMessage());

        }

    }

}
