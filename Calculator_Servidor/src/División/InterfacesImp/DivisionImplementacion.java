package InterfacesImp; //Agregar el paquete donde este la interfaz remota 

import Interfaces.Division;// Importar la clase Adder para que pueda ser utilizada 

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class DivisionImplementacion extends UnicastRemoteObject implements Division {

    public DivisionImplementacion() throws RemoteException {
        super();
    }

    @Override
    public int division(int a, int b) throws RemoteException {
        return a / b;
    }
}
