package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Division extends Remote {
    public int division(int a, int b) throws RemoteException;

}