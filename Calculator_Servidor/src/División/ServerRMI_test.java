import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import InterfacesImp.DivisionImplementacion;

public class ServerRMI_test {
    public static void main(String[] arg) {
        try {
            DivisionImplementacion divisionImplementacion = new DivisionImplementacion();
            Registry registry = LocateRegistry.createRegistry(9090);
            registry.rebind("Division", divisionImplementacion);
            System.out.println("Servidor corriendo");
        } catch (RemoteException e) {
            System.out.println(e.getMessage());

        }

    }

}